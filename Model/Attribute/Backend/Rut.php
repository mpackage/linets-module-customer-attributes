<?php

namespace Linets\CustomerAttributes\Model\Attribute\Backend;

class Rut extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
      $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return $this
     */
    public function beforeSave($object)
    {
        $this->validateRut($object);
        $this->checkIfIsDuplicated($object);

        return parent::beforeSave($object);
    }

    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function checkIfIsDuplicated($obj)
    {
        $userId = $obj->getEntityId();
        $attributeId = $this->getAttribute()->getAttributeId();
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $valor = $obj->getData($attributeCode);
        $rut = trim(str_replace('.', '', $valor));
        $rutQuoted = $this->resourceConnection->getConnection()->quote($rut);

        $customerEntityVarchar = $this->resourceConnection->getTableName('customer_entity_varchar');
        $sql = 'SELECT entity_id FROM ' . $customerEntityVarchar . ' WHERE attribute_id = ' . (int)$attributeId .
        ' AND REPLACE(value, ".", "") = ' . $rutQuoted .
        ' AND entity_id <> ' . (int)$userId;
        $query = $this->resourceConnection->getConnection()->fetchAll($sql);

        if (!$query && !count($query)) {
            return true;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Ya existe una persona registrada con el RUT "%1".
                Si esto es un error, por favor, comuníquese con nosotros.', $rut)
            );
        }
    }

    /**
     * Vaalida que el rut sea valido
     *
     * @param \Magento\Framework\DataObject $object
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateRut($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $valor = $object->getData($attributeCode);
        $rut = trim($valor);

        if ($rut == '') {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El RUT no puede estar vacio')
            );
        }

        if (!preg_match("/^[0-9]+[-]{1}+[0-9kK]{1}/", $rut)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El RUT "%1" es inválido', $rut)
            );
        }

        $rut = preg_replace('/[\.\-]/i', '', $rut);
        $dv = substr($rut, -1);
        $numero = substr($rut, 0, strlen($rut) - 1);
        $i = 2;
        $suma = 0;

        foreach (array_reverse(str_split($numero)) as $v) {
            if ($i == 8) {
                $i = 2;
            }

            $suma += $v * $i;
            ++$i;
        }

        $dvr = 11 - ($suma % 11);
        if ($dvr == 11) {
            $dvr = 0;
        }
        if ($dvr == 10) {
            $dvr = 'K';
        }
        if ($dvr != strtoupper($dv)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El RUT "%1" es inválido', $valor)
            );
        }
    }
}
