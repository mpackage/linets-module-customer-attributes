<?php

namespace Linets\CustomerAttributes\Model\Attribute\Backend;

class Telephone extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return $this
     */
    public function beforeSave($object)
    {
        $this->validateTelephone($object);

        return parent::beforeSave($object);
    }

    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateTelephone($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $valor = $object->getData($attributeCode);
        $telephone = trim($valor);

        if ($telephone == '') {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El Teléfono no puede estar vacio')
            );
        }

        if (strlen($telephone) < 8 || strlen($telephone) > 9) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El Teléfono "%1" debe tener entre 8 y 9 caracteres.', $telephone)
            );
        }
        if (!preg_match("/^[29][0-9]*$/", $telephone)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('El Teléfono "%1" es inválido', $telephone)
            );
        }
    }
}
