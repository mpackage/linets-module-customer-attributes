<?php
namespace Linets\CustomerAttributes\Plugin\Checkout;

use Magento\Customer\Model\Session;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;

class LayoutProcessor implements LayoutProcessorInterface
{
     /**
     * @var CheckoutHelper
     */
    protected $checkoutHelper;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Session
     */
    protected $session;

    /**
     * LayoutProcessor constructor.
     * @param CheckoutHelper $checkoutHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $session
     */
    public function __construct(
        CheckoutHelper $checkoutHelper,
        ScopeConfigInterface $scopeConfig,
        Session $session
    )
    {
        $this->checkoutHelper = $checkoutHelper;
        $this->scopeConfig = $scopeConfig;
        $this->session = $session;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $jsLayout = $this->shippingFields($jsLayout);
        $jsLayout = $this->billingFields($jsLayout);
        return $jsLayout;
    }


    protected function shippingFields($jsLayout){
        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
        $fields = $this->formFieldChanges($fields, 0, $jsLayout);
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $fields;

        return $jsLayout;
    }

    protected function billingFields($jsLayout){
        $isDisplayBillingOnPaymentMethodAvailable = $this->checkoutHelper->isDisplayBillingOnPaymentMethodAvailable();

        if (!$isDisplayBillingOnPaymentMethodAvailable) {
            $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields'];
            $fields['children'] = $this->formFieldChanges($fields['children'], 1, $jsLayout);
            // REPLACE CHILDREN
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields'] = $fields;
        } else {
            $paymentMethodRenders = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']
            ['children']['payment']['children']['payments-list']['children'];
            if (is_array($paymentMethodRenders)) {
                foreach ($paymentMethodRenders as $name => $renderer) {
                    if (isset($renderer['children']) && array_key_exists('form-fields', $renderer['children'])) {
                        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'];
                        $fields = $this->formFieldChanges($fields, 1, $jsLayout);
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'] = $fields;
                    }
                }
            }
        }

        return $jsLayout;
    }

    public function formFieldChanges($fields){
        if ($this->session->IsLoggedIn()){
            $customerData = $this->session->getCustomer()->getData();

            $fields['telephone']['value'] = $customerData['telephone'];
            $fields['telephone']['visible'] = false;

            $fields['rut']['value'] = $customerData['rut'];
            $fields['rut']['visible'] = false;

            $fields['firstname']['value'] = $customerData['firstname'];
            $fields['firstname']['visible'] = false;

            $fields['lastname']['value'] = $customerData['lastname'];
            $fields['lastname']['visible'] = false;  
        } else {
            $fields['rut']['sortOrder'] = 50;
            $fields['rut']['validation']['rutCL'] = true;
            $fields['rut']['validation']['required-entry'] = true;
            $fields['rut']['validation']['max_text_length'] = 10;
    
            $fields['telephone']['validation']['required-entry'] = true;
            $fields['telephone']['validation']['min_text_length'] = 8;
            $fields['telephone']['validation']['max_text_length'] = 9;
            $fields['telephone']['validation']['phoneCL'] = true;
        }

        return $fields;
    }
}
