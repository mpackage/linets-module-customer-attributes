<?php
namespace Linets\CustomerAttributes\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class RemoveCustomerAttributesPatch implements DataPatchInterface
{
        /**
         * @var CustomerSetupFactory
         */
        private $customerSetupFactory;
    
        /**
         * @var ModuleDataSetupInterface
         */
        private $setup;
    
        /**
         * AccountPurposeCustomerAttribute constructor.
         * @param ModuleDataSetupInterface $setup
         * @param CustomerSetupFactory $customerSetupFactory
         */
        public function __construct(
            ModuleDataSetupInterface $setup,
            CustomerSetupFactory $customerSetupFactory
        )
        {
            $this->customerSetupFactory = $customerSetupFactory;
            $this->setup = $setup;

        }
    
        public function apply()
        {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $this->setup]);
            //remove comuna customer address attribute
            $customerSetup->removeAttribute('customer_address', 'comuna');
            //remove rut customer attribute manually added 
            $customerSetup->removeAttribute('customer_address', 'rut');
            $customerSetup->removeAttribute('customer', 'rut');
        }
    
        public static function getDependencies()
        {
            return [];
        }
    
        public function getAliases()
        {
            return [];
        }
    }