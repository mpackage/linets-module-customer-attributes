<?php
namespace Linets\CustomerAttributes\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\CustomerGraphQl\Model\Resolver\CustomerAddresses;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallCustomerAttributesPatch implements DataPatchInterface
{
        /**
         * @var CustomerSetupFactory
         */
        private $customerSetupFactory;
    
        /**
         * @var ModuleDataSetupInterface
         */
        private $setup;
    
        /**
         * @var Config
         */
        private $eavConfig;

        /**
         * @var AttributeSetFactory
         */
        private $attributeSetFactory;
    
        /**
         * AccountPurposeCustomerAttribute constructor.
         * @param ModuleDataSetupInterface $setup
         * @param Config $eavConfig
         * @param CustomerSetupFactory $customerSetupFactory
         * @param AttributeSetFactory $attributeSetFactory
         */
        public function __construct(
            ModuleDataSetupInterface $setup,
            Config $eavConfig,
            CustomerSetupFactory $customerSetupFactory,
            AttributeSetFactory $attributeSetFactory
        )
        {
            $this->customerSetupFactory = $customerSetupFactory;
            $this->setup = $setup;
            $this->eavConfig = $eavConfig;
            $this->attributeSetFactory = $attributeSetFactory;
        }
    
        public function apply()
        {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $this->setup]);
            $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
            $attributeSetId = $customerSetup->getDefaultAttributeSetId($customerEntity->getEntityTypeId());
            $attributeGroup = $customerSetup->getDefaultAttributeGroupId($customerEntity->getEntityTypeId(), $attributeSetId);

            //add rut customer attribute
            $customerSetup->addAttribute(Customer::ENTITY, 'rut', [
                'type' => 'varchar',
                'input' => 'text',
                'label' => 'RUT',
                'required' => true,
                'visible' => true,
                'user_defined' => true,
                'system' => false,
                'position' => 50,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'backend' => '\Linets\CustomerAttributes\Model\Attribute\Backend\Rut'
            ]);
            $rutAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'rut');
            $rutAttribute->addData([
                'used_in_forms' => ['adminhtml_checkout','adminhtml_customer','customer_account_edit','customer_account_create'],
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroup
            ]);
            $rutAttribute->save();

            //add telephone customer attribute
            $customerSetup->addAttribute(Customer::ENTITY, 'telephone', [
                'type' => 'varchar',
                'input' => 'text',
                'label' => 'Teléfono',
                'required' => true,
                'visible' => true,
                'user_defined' => true,
                'system' => false,
                'position' => 60,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'backend' => '\Linets\CustomerAttributes\Model\Attribute\Backend\Telephone'
            ]);
            $telephoneAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'telephone');
            $telephoneAttribute->addData([
                'used_in_forms' => ['adminhtml_checkout','adminhtml_customer','customer_account_edit','customer_account_create'],
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroup
            ]);
            $telephoneAttribute->save();

            $customerAddressEntity = $customerSetup->getEavConfig()->getEntityType('customer_address');
            $attributeAddressSetId = $customerAddressEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeAddressGroupId = $attributeSet->getDefaultGroupId($attributeAddressSetId);

            // add rut customer address attribute
            $customerSetup->addAttribute('customer_address', 'rut', [
                'type' => 'varchar',
                'input' => 'text',
                'label' => 'RUT',
                'required' => false,
                'user_defined' => true,
                'system' => false,
                'position' => 100,
                'visible' => 1,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL
            ]);
            $rutAddressAttribute = $this->eavConfig->getAttribute('customer_address', 'rut');
            $rutAddressAttribute->addData([
                'used_in_forms' => ['adminhtml_customer_address','customer_address_edit','customer_register_address'],
                'attribute_set_id' => $attributeAddressSetId,
                'attribute_group_id' => $attributeAddressGroupId
            ]);
            $rutAddressAttribute->save();
        }
    
        public static function getDependencies()
        {
            return [
                RemoveCustomerAttributesPatch::class
            ];
        }
    
        public function getAliases()
        {
            return [];
        }
    }